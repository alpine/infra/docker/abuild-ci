# abuild-ci

This docker image is used to run the abuild test-suite. It also comes with a
docker-compose file to make it easy to test abuild locally in docker.

## Usage

The image has an entrypoint that will copy the sources to a workdir.

``` yaml
test:
  image: alpinelinux/abuild-ci
  script: [make && make check]
```

## Local testing

To run the test suite locally, first create a symlink called abuild to point to
the abuild source in the root of this project. Then run:

``` sh
docker compose run --rm test sh -c "make && make check"
docker compose run --rm test sh -c "make CC=clang"
docker compose run --rm test sh -c "build-scan --status-bugs make"
```
